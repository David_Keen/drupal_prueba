#!/usr/bin/env bash
# Environment:
#  OS - Ubuntu 18.04
#  PHP - 7.3.5 (but the container use PHP 7.1).
#
# Prerequisites:
#  Docker
#  Docker Compose

# 1- Upgrade the OS
cd ..
sudo apt-get -y upgrade
sudo apt update
 


# 2- Install and run Docker
sudo apt install -y build-essential apt-transport-https ca-certificates jq curl software-properties-common file
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install -y docker-ce
sudo chmod 666 /var/run/docker*
systemctl is-active docker
echo --------------------------------------------------------------DOCKER IS ACTIVE----------------------------------------------------------------------------------------------

 

# 3- Install and run Docker Compose
VERSION=$(curl --silent https://api.github.com/repos/docker/compose/releases/latest | jq .name -r)
DESTINATION=/usr/local/bin/docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/${VERSION}/docker-compose-$(uname -s)-$(uname -m) -o $DESTINATION
sudo chmod 755 $DESTINATION
docker-compose --version

 

# Steps to Install OpenEuropa Drupal 8 Site
# 4- Stop your Apache webserver
sudo /etc/init.d/apache2 stop

 

# 5- Clone the OpenEurope Drupal Project
git clone https://github.com/openeuropa/drupal-site-template.git

 

# 6- Run the project with Docker-Compose up
cd drupal-site-template
docker-compose up -d

# 7- Execute composer install in order to build all the web project
docker-compose exec web composer install

 

# 8- Get the ID/IP from web container and inspect it
docker ps
IDCONTAINER=$(docker ps | grep site-template_web | cut -d " " -f1)
echo Id = $IDCONTAINER
IPCONTAINER=$(docker container inspect $IDCONTAINER | grep '\"IPAddress\": \"')
IPCONTAINER=${IPCONTAINER##*: \"}
IPCONTAINER=${IPCONTAINER%\"*}
echo IP = $IPCONTAINER

# 9- Run the installer / Task Runner based in Robo (PHP)
docker exec $IDCONTAINER ./vendor/bin/run toolkit:install-clean

# 10- Export the configuration files from database to config/sync
docker exec $IDCONTAINER ./vendor/bin/drush cex


# 11- Install the sparql container for Open Europa Content Types
echo ------------------------------------------MODIFY JSON--------------easyrdf---------------------------------
sed  -i '/\"require\": {/a \\t\"easyrdf/easyrdf\": \"0.10.0-alpha.1 as 0.9.1\",' composer.json
echo ------------------------------------------COMPOSER UPDATE ON easyrdf---------------------------------------
docker-compose exec web composer update easyrdf/easyrdf 

echo "
  sparql:
    image: openeuropa/triple-store-dev
    environment:
    - SPARQL_UPDATE=true
    - DBA_PASSWORD=dba
    ports:
      - "8890:8890"
" >> docker-compose.yml

 

# 12- Add the connection to the new Sparql container
echo DRUPAL_SPARQL_HOST=sparql >> .env
sudo chmod 777 web/sites/default/settings.php
echo "
\$databases[\"sparql_default\"] = array(
  'default' => array(
    'prefix' => '',
    'host' => getenv('DRUPAL_SPARQL_HOST'),
    'port' => '8890',
    'namespace' => 'Drupal\\Driver\\Database\\sparql',
    'driver' => 'sparql'
  )
);
">> web/sites/default/settings.php
sudo chmod 755 web/sites/default/settings.php

 

# 13- Restart the whole Docker Compose Network, all the containers
docker-compose stop
docker-compose up -d

# 14- Install and enable some modules for working with Drupal
IDCONTAINER=$(docker ps | grep site-template_web | cut -d " " -f1)
echo Id = $IDCONTAINER

docker-compose exec web drush then bartik

docker exec $IDCONTAINER composer global require hirak/prestissimo
docker exec $IDCONTAINER composer require drupal/devel
docker exec $IDCONTAINER drush en devel
docker exec $IDCONTAINER composer require drupal/admin_toolbar
docker exec $IDCONTAINER drush en admin_toolbar
docker exec $IDCONTAINER drush en webprofiler

docker exec $IDCONTAINER drupal/sparql_entity_storage:dev-empty-module

# 15- Install and enable the Open Europa stable set of modules
docker exec $IDCONTAINER composer require openeuropa/rdf_skos:0.3.0 openeuropa/oe_content:1.0.0-beta3 openeuropa/oe_media:1.0.0-beta4

docker exec $IDCONTAINER composer require openeuropa/oe_search
docker exec $IDCONTAINER composer require openeuropa/oe_corporate_blocks
docker exec $IDCONTAINER composer require openeuropa/oe_multilingual
docker exec $IDCONTAINER composer require openeuropa/oe_theme:1.1.0

docker exec $IDCONTAINER drush en oe_media rdf_skos oe_content
docker exec $IDCONTAINER drush en -y oe_search oe_corporate_blocks oe_multilingual


IPCONTAINER=$(docker container inspect $IDCONTAINER | grep '\"IPAddress\": \"')
IPCONTAINER=${IPCONTAINER##*: \"}
IPCONTAINER=${IPCONTAINER%\"*}
echo IP = $IPCONTAINER

IDCONTAINER=$(docker ps | grep site-template_web | cut -d " " -f1)
echo Id = $IDCONTAINER

MODULES=$(docker exec $IDCONTAINER drush pm:list --status=disabled | grep -o "oe_.*)" | tr -d \) | tr '\n' '\ ')
docker exec $IDCONTAINER composer require drupal/styleguide
docker exec $IDCONTAINER drush en -y $MODULES

 

# 16- Create the basic structure folders / files for subtheming
mkdir lib
mkdir lib/themes
mkdir lib/modules
mkdir lib/themes/openeuropa_subtheme
mkdir lib/themes/openeuropa_subtheme/js
mkdir lib/themes/openeuropa_subtheme/css


#------------------------------------------------Config info.yml----------------------------------------------------------

echo 'name: Open Europa Subtheme
type: theme
description: This is an Open Europa Subtheme
core: 8.x
# His father theme
base theme: oe_theme
# Library definition
libraries:
  - openeuropa_subtheme/global-styling
  - openeuropa_subtheme/global-scripts
# Regions
regions:
  site_top_bar: "Site top bar"
  site_header: "Site header"
  page_header: "Page header"
  navigation: "Navigation"
  content: "Content"
  footer: "Footer"
' >> lib/themes/openeuropa_subtheme/openeuropa_subtheme.info.yml


# -------------------------------------------------------------Config libraries.yml----------------------------------------------------------------

echo 'global-styling:
  version: 1.x
  css:
    theme:
      css/openeuropa_subtheme.css: {}
global-scripts:
  version: 1.x
  js:
    js/openeuropa_subtheme.js: {}
  dependencies:
    - core/jquery
    - core/drupal
    - core/drupalSettings
    - core/jquery.once' >> lib/themes/openeuropa_subtheme/openeuropa_subtheme.libraries.yml

# ---------------------------------------------------------Config *.js----------------------------------------------------------------------

echo '(function ($){
console.log("hi");
})(jQuery);' >> lib/themes/openeuropa_subtheme/js/openeuropa_subtheme.js

# -----------------------------------------------------------Create css/style.css---------------------------------------------------------------------
touch lib/themes/openeuropa_subtheme/css/openeuropa_subtheme.css

# 17- Put the OpenEuropa website in dev mode, turning off the Cache system in Drupal
docker-compose exec web ./vendor/drupal/console/bin/drupal site:mode dev

# 18- Create a new site alias in Apache config with subdomain for the browser

# 19- Install and enable subtheme
docker-compose exec web drush then openeuropa_subtheme
docker-compose exec web drush config-set -y system.theme default openeuropa_subtheme

# 20- Open the new Drupal project from web container in browser and new prompt
if which xdg-open > /dev/null
then
  xdg-open "http://$IPCONTAINER:8080/web"
elif which gnome-open > /dev/null
then
  gnome-open "http://$IPCONTAINER:8080/web"
fi
gnome-terminal --command="docker exec -it $IDCONTAINER /bin/bash"
